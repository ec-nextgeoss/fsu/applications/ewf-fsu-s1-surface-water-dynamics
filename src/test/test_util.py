import sys
import os
import unittest
from io import BytesIO as StringIO
import cioppy

# Simulating the Runtime environment
os.environ['TMPDIR'] = '/tmp'
os.environ['_CIOP_APPLICATION_PATH'] = '/application'
os.environ['ciop_job_nodeid'] = 'dummy'
os.environ['ciop_wf_run_root'] = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'artifacts')


def log_input(reference):
    """
    Just logs the input reference, using the ciop.log function
    """
    ciop = cioppy.Cioppy()
    ciop.log('INFO', 'processing input: ' + reference)


class NodeATestCase(unittest.TestCase):
    
    def setUp(self):
        pass
    
    def test_log(self):
        # Example of data reference passed as input
        input_reference = 'https://catalog.terradue.com:443/sentinel1/search?' \
                          'format=json&' \
                          'uid=S1A_IW_GRDH_1SDV_20151204T181807_20151204T181832_008896_00CB99_1064'
        
        # The log function uses ciop.log, which writes on stderr.
        # Therefore we need to check the stderr to test the function.
        stderr = StringIO()
        sys.stderr = stderr
        log_input(input_reference)
        output = stderr.getvalue()
        log_message = output.split('\n')[1][27:]
        self.assertEqual(log_message,
                         '[INFO   ] [user process] processing input: ' + input_reference,
                         'Input reference not logged')


if __name__ == '__main__':
    unittest.main()
