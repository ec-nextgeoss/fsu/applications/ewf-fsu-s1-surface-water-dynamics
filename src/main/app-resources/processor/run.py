#!/opt/anaconda/envs/geowetlands/bin/python

import sys
sys.path.append('/opt/anaconda/envs/geowetlands/site-packages')

import atexit
import os
import tarfile
import cioppy
import paramiko

from pyroSAR import identify
from pyroSAR.snap import geocode
from pyroSAR.ancillary import parse_datasetname, find_datasets
from spatialist.ancillary import finder, run
from spatialist.vector import wkt2vector
from spatialist.raster import Raster, png

ciop = cioppy.Cioppy()

# define the exit codes
SUCCESS = 0
ERR_SNAP = 2
ERR_NOEXPR = 3
ERR_NOINPUT = 4


# add a trap to exit gracefully
def clean_exit(exit_code):
    log_level = 'INFO'
    if exit_code != SUCCESS:
        log_level = 'ERROR'
    
    msg = {SUCCESS: 'Processing successfully concluded',
           ERR_SNAP: 'SNAP_processor failed to process product',
           ERR_NOEXPR: 'No expression provided',
           ERR_NOINPUT: 'No input provided'}
    
    ciop.log(log_level, msg[exit_code])


def properties(src, dst=None):
    """
    Write a Java properties file for a SAR image processed by pyroSAR.

    Parameters
    ----------
    src: str
        the SAR image GeoTiff
    dst: str or None
        the name of the file to write. If `None`, the name is created by
        replacing the extension of `src`, e.g. `.tif`, with `.properties`.

    Returns
    -------

    """
    meta = parse_datasetname(src, parse_date=True)
    with Raster(src) as ras:
        wkt = ras.bbox().convert2wkt(set3D=False)[0]
    items = {
        'date': meta['start'].strftime('%Y-%m-%dT%H:%M:%SZ'),
        'title': os.path.splitext(os.path.basename(src))[0],
        'geometry': wkt
    }
    lines = ['{}={}'.format(k, v) for k, v in items.items()]
    if dst is None:
        dst = src.replace(os.path.splitext(src)[1], '.properties')
    with open(dst, 'w') as out:
        out.write('\n'.join(lines))


def sftp_upload(source, target, host, username, key):
    """
    Transfer files to an FTP server.
    
    Parameters
    ----------
    source: str
        the name of the files to be uploaded
    target: str
        the full name of the file on the FTP server
    host: str
        the FTP server address
    username: str
        the user name on the host
    key: str
        the RSA key file

    Returns
    -------

    """
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    k = paramiko.RSAKey.from_private_key_file(key)
    ssh.connect(host, username=username, pkey=k)
    sftp = ssh.open_sftp()
    sftp.put(localpath=source, remotepath=target)
    sftp.close()
    ssh.close()


def main():
    # create the output folder to store the output products and export it
    output_path = os.path.join(ciop.tmp_dir, 'output')
    os.makedirs(output_path)
    
    # Loops over all the inputs
    for inputfile in sys.stdin:
        # report activity in log
        ciop.log('INFO', 'The input file is: ' + inputfile)
        ciop.log('INFO', 'The temporary directory is: ' + ciop.tmp_dir)
        
        # retrieve the Sentinel-1 GRD product to the local temporary folder TMPDIR provided by the framework (this folder is only used by this process)
        # the ciop.copy function will use one of online resource available in the metadata to copy it to the TMPDIR folder
        # the funtion returns the local path so the variable retrieved contains the local path to the S1 product
        if inputfile[:4] == 'file':
            enclosure = inputfile
        else:
            out, err = run(['opensearch-client', inputfile, 'enclosure'], void=False)
            enclosure = out.rstrip()
        
        ciop.log('INFO', 'The download URL is: ' + enclosure)
        retrieved = ciop.copy(enclosure, ciop.tmp_dir)
        assert retrieved
        
        basename = os.path.basename(retrieved)
        retrieved = os.path.join(retrieved, basename + '.SAFE')
        
        ciop.log('INFO', 'Retrieved ' + retrieved)
        
        # bbox = ciop.getparam('qbbox').replace('%20', ' ')
        
        # process the scene to RTC gamma0 backscatter
        # with wkt2vector(bbox, srs=4326) as vec:
        #     geocode(infile=retrieved, outdir=ciop.tmp_dir, t_srs=4326, tr=100,
        #             scaling='dB', polarizations='VV', removeS1BorderNoiseMethod='ESA',
        #             shapefile=vec)
        geocode(infile=retrieved, outdir=ciop.tmp_dir, t_srs=4326, tr=100,
                scaling='dB', polarizations='VV', removeS1BorderNoiseMethod='ESA',
                removeS1ThermalNoise=False)
        
        tifs = find_datasets(ciop.tmp_dir, polarization=('VV', 'VH'))
        for tif in tifs:
            properties(tif)
            png_name = tif.replace('.tif', '.png')
            with Raster(tif) as ras:
                max_edge = 2000
                max_dim = max(ras.dim)
                if max_dim > max_edge:
                    percent = max_edge / max_dim * 100
                else:
                    percent = 100
                png(src=ras, dst=png_name, worldfile=True, percent=percent)
        
        # compress the processor results
        id = identify(retrieved)
        files = finder(ciop.tmp_dir, [id.outname_base()], regex=True)
        
        tarname = os.path.join(output_path, basename + '.tar.gz')
        with tarfile.open(tarname, 'w:gz') as tar:
            for item in files:
                arcname = os.path.relpath(item, start=ciop.tmp_dir)
                tar.add(item, arcname=arcname)
            tar.close()
        
        target = os.path.join('/datalake', basename + '.tar.gz')
        
        key = os.path.join(os.path.expanduser('~'), '.ssh', 'sftpkey')
        print('key for ftp transfer: {}'.format(key))
        
        ciop.log('INFO', 'Transferring result to remote FTP server')
        sftp_upload(source=tarname, target=target, host='87.254.4.230',
                    username='sftp-user', key=key)
        
        # publish the compressed results
        ciop.log('INFO', 'Publishing {}'.format(os.path.basename(tarname)))
        ciop.publish(tarname)


try:
    main()
except SystemExit as e:
    if e.args[0]:
        clean_exit(e.args[0])
    raise
else:
    atexit.register(clean_exit, 0)
