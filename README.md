NextGEOSS GEOWetlands Sentinel-1 GRD processing
===============================================

Installation
------------

* Run these commands in a shell:

```bash
# install ESA SNAP
sudo yum install snap6

# create a conda environment and install service dependencies
sudo conda create --name geowetlands
conda activate geowetlands
sudo conda install --name geowetlands cioppy
sudo conda install --name geowetlands --channel conda-forge pyroSAR

# clone the service repository and checkout the development branch
git clone https://gitlab.com/ec-nextgeoss/fsu/applications/ewf-fsu-s1-surface-water-dynamics.git
cd ewf-fsu-s1-surface-water-dynamics
git checkout develop

# deploy the service
mvn clean install -D service=processor -P python
```

Execution
---------

```bash
ciop-run --debug
```
